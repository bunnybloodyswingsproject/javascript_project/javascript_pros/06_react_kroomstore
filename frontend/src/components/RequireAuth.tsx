import React from 'react'
import { useLocation, Navigate, Outlet } from "react-router-dom";
import useAuth from '../hooks/useAuth';
import { User } from "../utils/Type_definition";
import { allowedRoles } from '../utils/Type_definition';

const RequireAuth: React.FC<allowedRoles> = ({ allowedRoles }) => {
    const { user } = useAuth();
    const location = useLocation();

    return (
        user?.roles?.find(role => allowedRoles?.includes(role))
        ? <Outlet />
        : user?.token
        ? <Navigate to="/unauthorized" state={{from: location}} replace />
        : <Navigate to="/login" state={{ from: location }} replace />
    )
}

export default RequireAuth