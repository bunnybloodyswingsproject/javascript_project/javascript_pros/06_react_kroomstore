import React, { ChangeEventHandler, useReducer, useRef } from 'react'
import { CameraDevices, AuthScannerProps, AuthScannerResultType } from '../utils/Type_definition';
import AuthScannerSelect from "./AuthScanneSelect";
import { NotFoundException } from "@zxing/library";
import useAuthScanner from '../hooks/useAuthScanner';
import axios from 'axios';
import useAuth from '../hooks/useAuth';
import { useLocation, useNavigate } from "react-router-dom"
const AuthScanner: React.FC<AuthScannerProps> = ({ selectedDeviceId, devices, setSelectedDeviceId, codeReader, setBarcode }) => {
  const videoRef = useRef<HTMLVideoElement>(null);
  const axiosInstance = axios.create({
    baseURL: import.meta.env.VITE_KROOM_SERVER_API
  });
  const location = useLocation();
  const from = location?.state?.from?.pathname || "/";
  const navigate = useNavigate();
  const { setIsOpenAuthscanner } = useAuthScanner();
  const { setUser } = useAuth();

  const handleStartAuthScanner = () => {
    codeReader.decodeFromVideoDevice(selectedDeviceId, videoRef.current, async (result, err) => {
      if(result) {
        setBarcode(result.getText());
        const scannedBarcode = result.getText();
        
        const response = await axiosInstance.post("/auth/login", 
          JSON.stringify({
            username: scannedBarcode
          }), {
            headers: { "Content-Type": "application/json" },
            withCredentials: true
          }
        );

        if(!response.data.success) {
          return alert(response.data.message);
        }

        setUser(response.data.user)
        codeReader.reset();
        setIsOpenAuthscanner(false);
        navigate(from, { replace: true })

      }

      if(err && !(err instanceof NotFoundException)) {
        console.log(err);
      }
    });

    
  }

  return (
    <div className="auth__scanner_container">
        <div id="reader">
          <div className='auth__scanner_video_container'>
            <video src="#" ref={videoRef}></video>
          </div>
          <div className="auth__scanner_forms">
            <div className="auth__scanner_form_inputs">
              <AuthScannerSelect 
                devicesList={devices}
                setSelectedDeviceId={setSelectedDeviceId}
                selectedDeviceId={selectedDeviceId}
              />
              {/* <select onChange={handleSelectChange} className="auth_scanner_selectInput"> */}
                            
                {/* {
                  devices?.length > 0 
                  ?
                  (
                    devices?.map((device: CameraDevices, key) => {
                      <option key={device?.deviceId} value={device?.deviceId}>{device?.label}</option>
                    })
                  )
                  :
                  (
                    <option value="">No available devices</option>
                  )
                }
                 */}
              {/* </select> */}
            </div>
            <div className="auth_scanner_form_btns">
              <button className="reset">RESET</button>
              <button className="start" onClick={handleStartAuthScanner}>START</button>
            </div>
          </div>
        </div>
    </div>
  )
}

export default AuthScanner