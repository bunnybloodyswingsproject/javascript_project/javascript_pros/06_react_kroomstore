import React from 'react'
import { Outlet } from 'react-router-dom';
import { useState, useEffect } from 'react';
import useRefreshToken from '../hooks/useRefreshToken';
import useAuth from '../hooks/useAuth';
import usePersistingAuth from '../hooks/usePersistingAuth';

const PersistLogin = () => {
    const [isLoading, setIsLoading] = useState(true);
    const refresh = useRefreshToken();
    const { user } = useAuth();
    const persistAuth = usePersistingAuth();

    useEffect(() => {
        
        const verifyRefreshToken = async () => {
            try {
                await refresh();
            }catch(error) {
                console.log(error);
            }finally {
                setIsLoading(false)
            }
        }

        if(!user.token) {
            verifyRefreshToken();
            persistAuth();
        }else{
            setIsLoading(false)
        }
    }, [])

    return (
        <>        
            {
                isLoading
                ? (
                    <p>Loading...</p>
                )
                : <Outlet />
            }
        </>
    )
}

export default PersistLogin