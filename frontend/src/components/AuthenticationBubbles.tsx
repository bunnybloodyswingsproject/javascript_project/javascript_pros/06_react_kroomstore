import React from 'react'
import Bubbles from './Bubbles'

const AuthenticationBubbles = () => {
  return (
    <div className="auth__bubbles">
        <div className="circle_blur1"></div>
        <div className="circle_blur2"></div>
        <div className="circle_blur3"></div>
        <Bubbles 
            bottom="24vh"
            left="10vw" 
            width="200px"
            height="200px"
            color="linear-gradient(299.57deg, #FFBF81 14.92%, rgba(255, 255, 255, 0.9) 79.4%)"
        />
        <Bubbles 
            bottom="17vh"
            right="17vw" 
            width="160px"
            height="160px"
            color="linear-gradient(118.48deg, #EE97CC 19.36%, rgba(255, 255, 255, 0.9) 87.56%)"
        />
    </div>
  )
}

export default AuthenticationBubbles