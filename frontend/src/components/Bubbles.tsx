import React from 'react'

type BubbleStyle = {
    bottom?: string;
    left?: string;
    width: string;
    height: string;
    right?: string;
    top?: string;
    color: string;
    transformProperty?: string;
}

const Bubbles: React.FC<BubbleStyle> = ({bottom, left, width, height, top, right, color, transformProperty}) => {
  return <div 
    className="bubbles"
    style={{
        bottom: bottom,
        left: left,
        width: width, 
        height: height,
        background: color,
        top: top,
        right: right,
        transform: transformProperty
    }}
></div>
}

export default Bubbles