import { ChangeEventHandler, SelectHTMLAttributes } from 'react';
import React, { useState, Dispatch, SetStateAction } from 'react';
import { CameraDevices } from '../utils/Type_definition';

interface Props extends SelectHTMLAttributes<HTMLSelectElement> {
  devicesList: CameraDevices[];
  setSelectedDeviceId: Dispatch<SetStateAction<string>>;
  selectedDeviceId: string
}

const DeviceSelect: React.FC<Props> = ({ devicesList, selectedDeviceId, setSelectedDeviceId, ...rest }) => {

  const handleSelectChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    const { value } = event.target;
    console.log(value)
    setSelectedDeviceId(value);
  };

  return (
    <select {...rest} value={selectedDeviceId} onChange={handleSelectChange}>
      
      {
        devicesList?.length >= 1
        ?
        (
            devicesList.map(device => (
                <option key={device.deviceId} value={device.deviceId}>
                {device.label}
                </option>
            ))
        )
        :
        (
            <option value="">No available devices</option>
        )
      }
    </select>
  );
};

export default DeviceSelect;