import { Dispatch, SetStateAction } from "react";
import { BrowserMultiFormatReader } from "@zxing/library";

export type AuthScannerContextType = boolean;
export interface AuthScannerType {
    isOpenAuthScanner: boolean; 
    setIsOpenAuthscanner: Dispatch<SetStateAction<boolean>>;
}

export interface CameraDevices {
    deviceId: string;
    groupId: string;
    kind: string;
    label: string
}

export type AuthScannerProps = {
    selectedDeviceId: string;
    devices: CameraDevices[];
    setSelectedDeviceId: Dispatch<SetStateAction<string>>;
    codeReader: BrowserMultiFormatReader;
    setBarcode: Dispatch<SetStateAction<string>>
}

export interface LoginProps {
    barcode: string
}

export interface AuthScannerResultType  {
    format: number;
    numbits: number;
    text: string;
    timestamp: number
}

export interface User {
    email: string,
    image: string,
    refreshToken: string,
    roles: string[],
    token: string,
    username: string,
    _id: string
}

export interface UserType {
    user: User,
    setUser: Dispatch<SetStateAction<User>>;       
}

export type allowedRoles = {
    allowedRoles: string[]
}

/* 
Role codes:
Admin: "5462"
Cashier: "5471"
Editor: "5479"
*/

export interface Product {
    _id: string,
    name: string,
    price: number,
    category: string,
    quantity: number
}

export interface ProductType {
    products: Product[],
    setProduct: Dispatch<SetStateAction<Product[]>>
}

export interface CartType {
    cart: Product[],
    setCart: Dispatch<SetStateAction<Product[]>>
}

export interface CartModalType {
    isOpenCartModal: boolean; 
    setIsOpenCartModal: Dispatch<SetStateAction<boolean>>;
}