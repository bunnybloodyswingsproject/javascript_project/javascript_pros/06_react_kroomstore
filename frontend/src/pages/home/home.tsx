import React, { ChangeEvent, MouseEventHandler, useEffect, useState } from 'react'
import "./home.css";
import { Product, ProductType } from '../../utils/Type_definition';
import useProducts from '../../hooks/useProducts';
import { axiosPrivate } from '../../api/axiosPrivate';
import useCart from '../../hooks/useCart';
import useCartModal from '../../hooks/useCartModal';
import useAuth from '../../hooks/useAuth';
const Home = () => {
  const [search, setSearch] = useState("");
  const [product_category, setProductCateogry] = useState("Fruits");
  const onHandleSearch = (e: ChangeEvent) => {
    const searchInput = e.target as HTMLInputElement;
    setSearch(searchInput.value);
  }
  const { user } = useAuth();
  const { products, setProduct } = useProducts();
  const { cart, setCart } = useCart();
  const [totalPrice, setTotalPrice] = useState(0);
  const { isOpenCartModal, setIsOpenCartModal } = useCartModal();
  const [current_link, setCurrentLink] = useState("");

  useEffect(() => {
    let isMounted = false;

    const getProducts = async () => {
      try {
        const response = await axiosPrivate.get("/products/",
          { params: { category: product_category } }
        );

        if(!response.data.success) {
          return console.log(response.data.message)
        }

        setProduct(response.data.products);
      }catch(error) {
        console.log(error);
      }
    }

    getProducts();

    return () => {
      isMounted = true;
    }

  }, [product_category])

  const cartToggle = (id?: string, method?: string) => {
    if(id === "0") {
      return;
    }
    if(method === "subtract") {
      const newCart = cart.map(prev => {
        if(prev._id === id) {
          if(prev.price * prev.quantity > prev.price) {
            setTotalPrice(currentPrice => currentPrice - prev.price);
          }
          return { ...prev, quantity: prev.quantity === 1 ? 1 : prev.quantity - 1 }
        }
        return prev;
      });

      setCart(newCart);
    }else{
      const newCart = cart.map(prev => {
        if(prev._id === id) {
          setTotalPrice(currentPrice => currentPrice + prev.price);
          return { ...prev, quantity: prev.quantity + 1 }
        }
        return prev
      });
      setCart(newCart)
    }
  }

  const addToCart = (product: Product) => {
    let alreadyOnCart: any = [];

    if(cart[0]._id === "0") {
      setCart([product]);
        setTotalPrice(prev => prev + product.price);
      return;
    }

    cart.find(validating_product => validating_product._id === product._id && alreadyOnCart.push(validating_product));

    if(alreadyOnCart.length === 0) {
      setCart(prev => [...prev, product]);
        setTotalPrice(prev => prev + product.price);

      return;
    }

    const newGoods = cart.map(goods => {
      if(product._id === goods._id) {
        const newPrice = goods.price + product.price;
        const newQuantity = goods.quantity+=1;
        return { ...goods, price: newPrice, quantity: newQuantity }
      }

      return goods;
    });
    // console.log(newGoods[newGoods.length - 1]);
    // cart.find(currentProduct => currentProduct._id === product._id && console.log(newGoods[newGoods.length - 1]))
    
    setCart(newGoods);
    let total: number = 0;

    newGoods.map(newCart => {
      let subtotal = 0;

      subtotal = newCart.price * newCart.quantity;
      total += subtotal
    });

    setTotalPrice(total);
  }

  const handleManualCheckout = async (e: any) => {
    e.preventDefault();

    try {
      const response = await axiosPrivate.post("/payments/manual_checkout/",
      JSON.stringify({
        userId: user._id,
        products: cart
      }), {
        headers: { "Content-Type": "application/json" },
        withCredentials: true
      }
      );

      if(!response.data.success) {
        console.log(response.data.message);
      }

      setCart([{
        _id: "0",
        name: "",
        price: 0,
        category: "",
        quantity: 0
      }]);
      setTotalPrice(0)
      setIsOpenCartModal(false);      
    }catch(error) {
      console.log(error);
    }
  }


  return (
    <>
    <div className={isOpenCartModal ? "kroom__modal_bg" : "kroom__modal_close"}>
      <div className="kroom__modal_container">
        <div className="kroom__modal_box">
          <div className="kroom__modal_header">
            <div className="cart__modal_toggleExit" onClick={() => setIsOpenCartModal(prev => !prev)}>
              <i className="fa-solid fa-chevron-left"></i>
            </div>
            <h4>Checkout</h4>
          </div>
          <div className="kroom_payment_option">
            <div className="kroom__onlinepay_option">
              <div className="payment__options">
                <div className="option_brand">
                  <img src="https://firebasestorage.googleapis.com/v0/b/krooma-store.appspot.com/o/default_images%2Fstripe__logo.svg?alt=media&token=0f3d3d8a-4389-4472-bc8e-4845b556580f" alt="payment_options" />
                  <div className="payment__option_description">
                    <p>Stripe</p>
                    <small>Pay with your stripe account</small>
                  </div>
                </div>
                <div className="option_cost">
                  <p>₱ {totalPrice}</p>
                </div>
              </div>
              <div className="payment__options">
                <div className="option_brand">
                  <img src="https://firebasestorage.googleapis.com/v0/b/krooma-store.appspot.com/o/default_images%2Fpaypal__logo.svg?alt=media&token=90ea397f-395e-4b50-bda8-bf3551376a0b" alt="payment_options" />
                  <div className="payment__option_description">
                    <p>Paypal</p>
                    <small>Pay with your paypal account</small>
                  </div>
                </div>
                <div className="option_cost">
                  <p>₱ {totalPrice}</p>
                </div>
              </div>
            </div>
            <div className="kroom__cart_total">
              <div className="option__total_bills">
                <small>Total</small>
                <small>₱ {totalPrice}</small>
              </div>
              <div className="option__total_bills withTax">
                <small>Tax 	&#40;If Applicable &#41;</small>
                <small>-</small>
              </div>
            </div>
            <div className="kroom__payment_terms">
              <small>Before proceeding to checkout, please take a moment to review your cart and ensure that all of the items you wish to purchase are included. If you need to make any changes, you can do so now. Thank you for shopping with us!</small>
            </div>
          </div>
          <div className="kroom_payment_modalBtn">
            <button onClick={handleManualCheckout}>checkout</button>
          </div>
        </div>
      </div>
    </div> 
    <div className="kroom">
      <div className='kroom__page_container'>
        <div className="kroom__nav_sidebar">
          <div className="sidebar__header">
              <img src="https://firebasestorage.googleapis.com/v0/b/krooma-store.appspot.com/o/default_images%2Fbrandkroom_brand_logo.svg?alt=media&token=9984521d-9c9b-470c-9c1d-aa5ec58c263d" alt="" />
          </div>
          <div className="sidebar__category_links">
            <div className="category_one_container">
              <svg width="53" height="45" viewBox="0 0 53 45" fill="none" xmlns="http://www.w3.org/2000/svg" className='icon__cateogry'>
                <path d="M11.0724 6.68931L11.0734 6.68886L19.2392 2.98808C19.2399 2.98778 19.2405 2.98747 19.2412 2.98717C21.0878 2.1562 23.2043 1.83336 25.2904 2.08155C25.2909 2.0816 25.2914 2.08166 25.2919 2.08172L34.4527 3.18749L34.4558 3.18787C36.5447 3.43665 38.442 4.23516 39.9027 5.4368L39.9031 5.43712L46.5493 10.9019C46.5494 10.902 46.5496 10.9022 46.5497 10.9023C48.007 12.1013 48.9216 13.606 49.2513 15.1917C49.2513 15.1919 49.2513 15.192 49.2513 15.1921L50.8632 22.9536L50.8637 22.9556C51.1906 24.5216 50.9289 26.1606 50.0824 27.6347L50.0811 27.637L46.0291 34.7145L46.0282 34.7159C45.1761 36.2072 43.7591 37.4819 41.9276 38.3107L41.9266 38.3111L33.7608 42.0119C33.7602 42.0122 33.7595 42.0125 33.7588 42.0128C31.9118 42.844 29.7947 43.1668 27.7081 42.9183L18.5453 41.8216L18.5442 41.8215C16.4553 41.5727 14.558 40.7742 13.0973 39.5726L13.0969 39.5723L6.43965 34.0981C4.98244 32.8993 4.06785 31.3949 3.73794 29.8094C3.73784 29.8089 3.73773 29.8084 3.73763 29.8079L2.13733 22.0491L2.13633 22.0444C1.80938 20.4784 2.07115 18.8394 2.91765 17.3653L2.91951 17.362L6.96965 10.2784C6.96988 10.278 6.97012 10.2776 6.97035 10.2772C7.82283 8.79332 9.24091 7.51813 11.0724 6.68931ZM24.9931 11.3549C23.9397 10.4606 22.5717 10.0009 21.2004 10.0009C19.8291 10.0009 18.461 10.4606 17.4077 11.3549C16.3444 12.2577 15.6673 13.5592 15.6673 15.0007C15.6673 16.4422 16.3444 17.7436 17.4077 18.6464C18.461 19.5408 19.8291 20.0004 21.2004 20.0004C22.5717 20.0004 23.9397 19.5408 24.9931 18.6464C26.0564 17.7436 26.7335 16.4422 26.7335 15.0007C26.7335 13.5592 26.0564 12.2577 24.9931 11.3549ZM13.8746 26.3536C12.8113 27.2564 12.1342 28.5578 12.1342 29.9993C12.1342 31.4408 12.8113 32.7423 13.8746 33.6451C14.928 34.5394 16.296 34.9991 17.6673 34.9991C19.0386 34.9991 20.4066 34.5394 21.46 33.6451C22.5233 32.7423 23.2004 31.4408 23.2004 29.9993C23.2004 28.5578 22.5233 27.2564 21.46 26.3536C20.4066 25.4592 19.0386 24.9996 17.6673 24.9996C16.296 24.9996 14.928 25.4592 13.8746 26.3536ZM42.6585 23.3539C41.6051 22.4595 40.2371 21.9999 38.8658 21.9999C37.4945 21.9999 36.1264 22.4595 35.0731 23.3539C34.0098 24.2567 33.3327 25.5581 33.3327 26.9996C33.3327 28.4411 34.0098 29.7425 35.0731 30.6453C36.1264 31.5397 37.4945 31.9993 38.8658 31.9993C40.2371 31.9993 41.6051 31.5397 42.6585 30.6453C43.7218 29.7425 44.3989 28.4411 44.3989 26.9996C44.3989 25.5581 43.7218 24.2567 42.6585 23.3539Z" fill="#FFBF81" stroke="#060606" strokeWidth={"4"}/>
              </svg>
              <svg width="53" height="45" viewBox="0 0 53 45" fill="none" xmlns="http://www.w3.org/2000/svg" className='icon__cateogry'>
                <path d="M21.5545 2.11855C21.5539 2.12047 21.5524 2.1249 21.5487 2.13199C21.5485 2.13228 21.5484 2.13258 21.5482 2.13288L14.0423 15.9185L12.4326 18.8749H15.7988H37.2012H40.5674L38.9577 15.9185L31.4518 2.13288C31.4516 2.13257 31.4515 2.13227 31.4513 2.13196C31.4436 2.1174 31.443 2.11043 31.4429 2.10927L31.4429 2.10918C31.4427 2.10688 31.4428 2.10283 31.4448 2.09667C31.4468 2.0905 31.4511 2.08165 31.4599 2.07148C31.4679 2.06226 31.4842 2.0466 31.5169 2.03044C31.5841 1.99726 31.6644 1.99036 31.7415 2.01362C31.8184 2.0368 31.8479 2.07499 31.8571 2.09169C31.8571 2.09174 31.8571 2.09179 31.8572 2.09184L40.4225 17.8309L40.9906 18.8749H42.1792H50.0556C50.667 18.8749 51 19.3217 51 19.6874C51 20.0531 50.667 20.4999 50.0556 20.4999H48.5118L48.1208 21.9933L43.3453 40.2307L43.3448 40.2326C42.9353 41.8024 41.4184 43 39.566 43H13.434C11.5816 43 10.0647 41.8024 9.65523 40.2326L9.65473 40.2307L4.87921 21.9933L4.48817 20.4999H2.94444C2.33297 20.4999 2 20.0531 2 19.6874C2 19.3217 2.33297 18.8749 2.94444 18.8749H10.8208H12.0094L12.5775 17.8309L21.1428 2.09184C21.1429 2.09176 21.1429 2.09169 21.143 2.09161C21.1503 2.07825 21.1798 2.03833 21.2608 2.01396C21.3425 1.9894 21.4201 1.99907 21.477 2.02743L22.3564 0.261586L21.477 2.02743C21.5082 2.04297 21.5252 2.05875 21.5347 2.06971C21.545 2.08161 21.5504 2.09252 21.553 2.1005C21.5556 2.10838 21.5555 2.11295 21.5553 2.11438C21.5553 2.11519 21.5551 2.11645 21.5545 2.11855ZM19.6667 35.1562V26.7187C19.6667 24.7553 18.0213 23.3124 16.1944 23.3124C14.3675 23.3124 12.7222 24.7553 12.7222 26.7187V35.1562C12.7222 37.1196 14.3675 38.5625 16.1944 38.5625C18.0213 38.5625 19.6667 37.1196 19.6667 35.1562ZM29.9722 26.7187C29.9722 24.7553 28.3269 23.3124 26.5 23.3124C24.6731 23.3124 23.0278 24.7553 23.0278 26.7187V35.1562C23.0278 37.1196 24.6731 38.5625 26.5 38.5625C28.3269 38.5625 29.9722 37.1196 29.9722 35.1562V26.7187ZM40.2778 35.1562V26.7187C40.2778 24.7553 38.6324 23.3124 36.8056 23.3124C34.9787 23.3124 33.3333 24.7553 33.3333 26.7187V35.1562C33.3333 37.1196 34.9787 38.5625 36.8056 38.5625C38.6324 38.5625 40.2778 37.1196 40.2778 35.1562Z" fill="#E9211C" stroke="black" strokeWidth={"2"}/>
              </svg>
              <svg width="53" height="45" viewBox="0 0 53 45" fill="none" xmlns="http://www.w3.org/2000/svg" className='icon__cateogry'>
                <path d="M14.9139 15.0594C17.8419 11.0642 22.6671 8.43505 28.1562 8.43505H38.9219C43.2716 8.43505 47.1706 6.43971 49.6614 3.30889C50.5286 6.18165 51 9.283 51 12.5211C51 27.8535 40.5348 39.7199 28.2545 39.7863H28.1562C20.5886 39.7863 14.2788 34.7847 12.4686 28.088L11.6193 24.9462L9.20009 27.1232C5.3785 30.562 2.96875 35.4904 2.96875 40.9828V42.5897C2.96875 42.7642 2.81405 43 2.48438 43C2.1547 43 2 42.7642 2 42.5897V40.9828C2 34.165 5.581 28.1385 11.055 24.5947C13.0829 23.2865 15.3451 22.3262 17.7882 21.7859C19.2686 21.4639 20.7915 21.29 22.3594 21.29H29.8125H31.4688C33.4268 21.29 35.125 19.7281 35.125 17.6832C35.125 15.6382 33.4268 14.0763 31.4688 14.0763H22.3594C19.7811 14.0763 17.2847 14.4174 14.9139 15.0594Z" fill="#9DCA41" stroke="black" strokeWidth={"2"}/>
              </svg>
            </div>
            <div className="category_two_container">
              <svg width="53" height="41" viewBox="0 0 53 41" fill="none" xmlns="http://www.w3.org/2000/svg" className='icon__cateogry'>
                <path d="M43.6312 3.01603L43.6388 3.01423C44.1935 2.88374 44.7967 2.8125 45.4286 2.8125C47.129 2.8125 48.5939 3.32871 49.5876 4.06698C50.5822 4.80587 51 5.6667 51 6.4375C51 6.7526 50.9379 7.04866 50.8171 7.33199L50.814 7.33928L50.8109 7.34659L50.8009 7.37074C50.3988 8.33503 49.8993 9.53302 49.6094 10.7701C49.3135 12.0332 49.1907 13.5104 49.6992 15.0018L49.7008 15.0065C51.5692 20.4433 48.7489 26.7565 42.3193 31.5332C35.9495 36.2656 26.9044 38.742 18.6044 37.1768C15.4648 36.5766 12.0747 37.3608 9.52608 37.9504C9.47729 37.9616 9.4288 37.9729 9.38064 37.984L9.38014 37.9841C8.80781 38.1165 8.20198 38.1875 7.57143 38.1875C5.87104 38.1875 4.40613 37.6713 3.41239 36.933C2.41783 36.1941 2 35.3333 2 34.5625C2 34.2583 2.06131 33.9621 2.18292 33.6768L2.18602 33.6695L2.18907 33.6622L2.19913 33.6381C2.60117 32.6738 3.10065 31.4758 3.39055 30.2387C3.68653 28.9756 3.80926 27.4984 3.30083 26.007L3.30027 26.0054C1.44068 20.566 4.26418 14.2425 10.6925 9.46676C17.0625 4.73434 26.1078 2.25787 34.408 3.82326C37.5474 4.42334 40.9373 3.63916 43.4858 3.04964C43.5346 3.03835 43.583 3.02714 43.6312 3.01603ZM25.565 11.1515L25.5665 11.1512C26.4568 10.9566 27.311 10.4807 27.8811 9.70858C28.4771 8.9016 28.7188 7.7986 28.2886 6.71205C27.8879 5.69987 27.0557 5.07364 26.2729 4.75461C25.4736 4.42882 24.5476 4.34008 23.6495 4.53598C15.4888 6.31204 8.35481 11.4494 5.78222 17.9636L5.77747 17.9756L5.77288 17.9877C5.3655 19.0596 5.60054 20.1493 6.18918 20.9543C6.75746 21.7315 7.61441 22.2144 8.52132 22.4047C9.40979 22.591 10.3289 22.5041 11.126 22.1813C11.9065 21.8652 12.7432 21.2422 13.1457 20.2254L13.1467 20.2231C14.7263 16.2184 19.5515 12.4607 25.565 11.1515Z" fill="#FBC90F" stroke="black" strokeWidth={"2"}/>
              </svg>
              <svg width="53" height="45" viewBox="0 0 53 45" fill="none" xmlns="http://www.w3.org/2000/svg" className='icon__cateogry'>
                <path d="M11.9134 15.1891L11.915 15.1886C13.0682 14.8459 14.1869 14.3737 15.0636 13.7483C15.8741 13.1702 16.9062 12.1412 16.9062 10.626V4.92188C16.9062 4.13661 16.52 3.5646 16.1893 3.23414C15.8654 2.91049 15.4974 2.71001 15.2013 2.58432C14.6011 2.32952 13.91 2.21875 13.25 2.21875C12.7224 2.21875 12.2376 2.17757 11.8096 2.10937C12.2376 2.04118 12.7224 2 13.25 2H14.9062H38.0938H39.75C40.2776 2 40.7624 2.04118 41.1904 2.10938C40.7624 2.17757 40.2776 2.21875 39.75 2.21875C39.09 2.21875 38.3989 2.32952 37.7987 2.58432C37.5026 2.71001 37.1346 2.91049 36.8107 3.23414C36.48 3.5646 36.0938 4.13661 36.0938 4.92188V10.626C36.0938 12.1412 37.1259 13.1702 37.9364 13.7483C38.8131 14.3737 39.9318 14.8459 41.085 15.1886L41.0866 15.1891C44.3641 16.1601 46.9396 17.4159 48.6518 18.7783C50.378 20.1521 51 21.4352 51 22.5V39.375C51 39.4658 50.9648 39.7259 50.5416 40.1502C50.1106 40.5821 49.3868 41.063 48.334 41.51C46.2362 42.4006 43.2011 43 39.75 43H13.25C9.79886 43 6.76378 42.4006 4.66597 41.51C3.61317 41.063 2.88937 40.5821 2.45843 40.1502C2.03515 39.7259 2 39.4658 2 39.375V22.5C2 21.4352 2.62196 20.1521 4.34824 18.7783C6.06036 17.4159 8.63591 16.1601 11.9134 15.1891ZM14.5625 29.6191C14.5625 30.8965 15.2204 31.9086 15.9731 32.6169C16.7216 33.3213 17.6967 33.863 18.7349 34.2758C20.8225 35.106 23.5691 35.5742 26.5 35.5742C29.4309 35.5742 32.1775 35.106 34.2651 34.2758C35.3033 33.863 36.2784 33.3213 37.0269 32.6169C37.7796 31.9086 38.4375 30.8965 38.4375 29.6191C38.4375 28.9348 38.1811 28.3692 37.9536 27.9872C37.7161 27.5883 37.414 27.2296 37.1146 26.9201C36.5159 26.3011 35.7392 25.6865 34.9443 25.1209C33.3409 23.9802 31.3875 22.8466 29.9081 22.0315C28.8833 21.4663 27.604 21.269 26.5104 21.269C25.4178 21.269 24.1399 21.4659 23.1155 22.0299C21.6235 22.8462 19.6655 23.9794 18.0596 25.12C17.2631 25.6857 16.4855 26.3004 15.8862 26.9196C15.5866 27.2292 15.2842 27.588 15.0466 27.9869C14.8189 28.3691 14.5625 28.9348 14.5625 29.6191Z" fill="white" stroke="black" strokeWidth={"2"}/>
              </svg>
            </div>
          </div>
          <div className="sidebar__user_settings">
            <img src="https://firebasestorage.googleapis.com/v0/b/krooma-store.appspot.com/o/default_images%2FGroup%203users_logo.svg?alt=media&token=767ae2e1-ca64-4170-b9ed-76e76624b0cf" alt="" />
          </div>
        </div>
        {/* Product listing */}
        <div className="kroom__product_container">
          <div className="kroom__product_searchBar">
            <div className="kroom_search_container">
              <form className="kroom_searcj_form_control">
                <input 
                  type="text" 
                  onChange={onHandleSearch}
                />
                <div className="search_icon_controller">
                  <i className="fa-solid fa-magnifying-glass"></i>
                </div>
              </form>
            </div>
          </div>
          <div className="kroom__product_card_container">
            <div className="product_list_container">
              {
                products[0]?._id === "0" ?
                <div className="empty__container">
                  <i className="fa-solid fa-circle-exclamation"></i>
                  <h3>EMPTY</h3>
                </div>
                :
                products?.map(product => (
                  <div className="product_card" key={product._id} onClick={() => addToCart(product)}>
                    <div className="card__image_container">
                      <img src="https://firebasestorage.googleapis.com/v0/b/krooma-store.appspot.com/o/default_images%2F5a5f62adee40df432bfac55a%202.png?alt=media&token=30bfdcd7-b5fb-422e-b236-b88846fba73e" alt="product_card_img" />
                    </div>
                    <div className="card__description">
                      <p>{product.name}</p>
                      <p>₱ {product.price}</p>
                      <small>{product.category}</small>
                    </div>
                  </div>
                ))
              }

            </div>
          </div>
        </div>
        {/* Cart Container */}
        <div className="kroom__main_cart_container">
          <div className="kroom__cart_content">
            <div className="cart__header">
              <p>Order details</p>
              <button>void</button>
            </div>
            <div className="cart__container_list">
              <div className="cart_product_container">
                {
                  cart?.map(cart_product => (
                    <div className="cart__product" key={cart_product._id}>
                      <div className="cart__image_container">
                        <img src="https://firebasestorage.googleapis.com/v0/b/krooma-store.appspot.com/o/default_images%2F5a5f62adee40df432bfac55a%202.png?alt=media&token=30bfdcd7-b5fb-422e-b236-b88846fba73e" alt="cart__img" />
                      </div>
                      <div className="cart__product_info">
                        <p>{cart_product.name}</p>
                        <div className="cart__price_and_quantity">
                          <div className="cart__toggle_action">
                            <button className="subtract" onClick={() => cartToggle(cart_product._id, "subtract")}>-</button>
                            <span>{cart_product.quantity}</span>
                            <button className="add" onClick={() => cartToggle(cart_product._id, "add")}>+</button>
                          </div>
                          <small>₱ {cart_product.price * cart_product.quantity}</small>
                        </div>
                      </div>
                    </div>
                  ))
                }
              </div>
            </div>
            <div className="kroom__cart_shop_summary">
              <p>order summary</p>
              <h2>₱ {totalPrice}</h2>
              <button onClick={() => {
                if(cart[0]._id === "0") {
                  return alert("Please add a product yo tour cart.")
                }

                setIsOpenCartModal(prev => !prev)
              }}>end transaction</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    </>
  )
}

export default Home