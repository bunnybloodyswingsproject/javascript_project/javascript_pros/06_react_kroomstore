import React, { useEffect, useRef, useState } from 'react'
import AuthenticationBubbles from '../../components/AuthenticationBubbles'
import "./login.css";
import useAuthScanner from '../../hooks/useAuthScanner';
import { LoginProps } from '../../utils/Type_definition';

const login: React.FC<LoginProps> = ({ barcode }) => {
  

  const { isOpenAuthScanner, setIsOpenAuthscanner } = useAuthScanner();
  const handleAuthScanner = () => {
      setIsOpenAuthscanner(prev => !prev);
  }

  return (

    <div className="authentication">
        <form className="authentication__form">
          <div className="auth__form_group" id="auth__brand">
            <img src="https://firebasestorage.googleapis.com/v0/b/krooma-store.appspot.com/o/default_images%2F91ff3a1de787cd72f42e156898463826-removebg-preview.png?alt=media&token=ca65d27a-edb6-4efa-bc12-8246a312f59e" alt="brand__logo" />
          </div>
          <div className="auth__form_group">
            <div className="auth__greetings">
              <p>hello</p>
              <h1>cashier!!!</h1>
            </div>
            <div className="auth__form_inputs">
              <input 
                type="text" 
                disabled={true}
                placeholder={barcode ? barcode : "xxx-xxxxx-xxxxxx"}
              />
              <div className="barcodes"
                   onClick={handleAuthScanner}
              >
                <i className="fa-solid fa-barcode"></i>
                <i className="fa-solid fa-barcode"></i>
                <i className="fa-solid fa-barcode"></i>
                <i className="fa-solid fa-barcode"></i>
                <i className="fa-solid fa-barcode"></i>
              </div>
            </div>
          </div>
        </form>
    </div>
  )
}

export default login