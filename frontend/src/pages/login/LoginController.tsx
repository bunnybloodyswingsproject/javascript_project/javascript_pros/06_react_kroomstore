import React from 'react'
import useAuthScanner from '../../hooks/useAuthScanner'

const LoginController = () => {
    const { setIsOpenAuthscanner } = useAuthScanner();
    const handleAuthScanner = () => {
        setIsOpenAuthscanner(prev => !prev)
    }
}

export default LoginController