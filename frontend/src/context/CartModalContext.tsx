import React, { createContext, useState } from "react";
import { CartModalType } from "../utils/Type_definition";
export const CartModalDefaultValue: CartModalType = {
    isOpenCartModal: false,
    setIsOpenCartModal: () => false
}
export const CartModalContext = createContext<CartModalType>(CartModalDefaultValue);
export const CartModalProvider = ({children}: {children?: React.ReactNode}) => {
    const [isOpenCartModal, setIsOpenCartModal] = useState<boolean>(false);

    return (
        <CartModalContext.Provider value={{ isOpenCartModal, setIsOpenCartModal }}>
            { children }
        </CartModalContext.Provider>
    )
}