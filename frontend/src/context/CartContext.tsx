import React, { createContext, useState } from "react";
import { CartType, Product } from "../utils/Type_definition";

export const CartModelDefault: CartType = {
    cart: [{
        _id: "0",
        name: "",
        price: 0,
        category: "",
        quantity: 0
    }],
    setCart: () => false
}

export const CartContext = createContext<CartType>(CartModelDefault);
export const CartProvider = ({children}: {children?: React.ReactNode}) => {
    const [cart, setCart] = useState<Product[]>([{
        _id: "0",
        name: "",
        price: 0,
        category: "",
        quantity: 0
    }]);

    return (
        <CartContext.Provider value={{ cart, setCart }}>
            { children }
        </CartContext.Provider>
    )
}