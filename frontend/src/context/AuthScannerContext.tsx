import React, { createContext, useState } from "react";
import { AuthScannerContextType, AuthScannerType } from '../utils/Type_definition';
export const AuthScannerDefaultValue: AuthScannerType = {
    isOpenAuthScanner: false,
    setIsOpenAuthscanner: () => false
}
export const AuthScannerContext = createContext<AuthScannerType>(AuthScannerDefaultValue);
export const AuthScannerProvider = ( {children}: {children?: React.ReactNode} ) => {
    const [isOpenAuthScanner, setIsOpenAuthscanner] = useState<AuthScannerContextType>(false);

    return (
        <AuthScannerContext.Provider value={{ isOpenAuthScanner, setIsOpenAuthscanner }}>
            { children }
        </AuthScannerContext.Provider>
    )
}