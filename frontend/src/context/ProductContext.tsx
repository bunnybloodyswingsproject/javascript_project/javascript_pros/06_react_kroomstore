import React, { createContext, useState } from 'react';
import { ProductType, Product } from '../utils/Type_definition';

export const ProductModelDefault: ProductType = {
    products: [{
        _id: 0,
        name: "",
        price: 0,
        category: "",
        quantity: 0
    }],
    setProduct: () => false
}

export const ProductContext = createContext<ProductType>(ProductModelDefault);
export const ProductProvider = ({children}: {children?: React.ReactNode}) => {
    const [products, setProduct] = useState<Product[]>([{
        _id: 0,
        name: "",
        price: 0,
        category: "",
        quantity: 0
    }]);

    return (
        <ProductContext.Provider value={{ products, setProduct }}>
            { children }
        </ProductContext.Provider>
    )
}