import React, { createContext, useState } from 'react';
import { User, UserType } from '../utils/Type_definition';
export const UserAuthDefault: UserType = {
    user: {
        email: "",
        image: "",
        refreshToken: "",
        roles: [],
        token: "",
        username: "",
        _id: ""
    },
    setUser: () => false
}
export const UserAuthContext = createContext<UserType>(UserAuthDefault);
export const UserAuthProvider = ({children}: {children?: React.ReactNode}) => {
    const [user, setUser] = useState<User>({
        email: "",
        image: "",
        refreshToken: "",
        roles: [],
        token: "",
        username: "",
        _id: ""
    });

    return (
        <UserAuthContext.Provider value={{ user, setUser }}>
            { children }
        </UserAuthContext.Provider>
    )
}