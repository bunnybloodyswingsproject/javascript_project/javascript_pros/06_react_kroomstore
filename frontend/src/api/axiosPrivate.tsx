import axios from 'axios';

export default axios.create({
    baseURL: import.meta.env.VITE_KROOM_SERVER_API
});

export const axiosPrivate = axios.create({
    baseURL: import.meta.env.VITE_KROOM_SERVER_API,
    headers: { "Content-Type": "application/json" },
    withCredentials: true
});
