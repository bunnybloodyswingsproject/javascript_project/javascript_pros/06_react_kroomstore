import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import { AuthScannerProvider } from './context/AuthScannerContext'
import './index.css'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import {UserAuthContext, UserAuthProvider} from './context/UserAuthContext'
import { ProductProvider } from './context/ProductContext'
import { CartProvider } from './context/CartContext'
import { CartModalProvider } from './context/CartModalContext'
ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <AuthScannerProvider>
      <UserAuthProvider>
        <ProductProvider>
          <CartProvider>
            <CartModalProvider>              
              <Router>
                <Routes>
                  <Route path='/*' element={ <App /> } />
                </Routes>
              </Router>
            </CartModalProvider>
          </CartProvider>
        </ProductProvider>
      </UserAuthProvider>
    </AuthScannerProvider>
  </React.StrictMode>,
)
