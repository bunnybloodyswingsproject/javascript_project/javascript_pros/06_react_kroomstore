import axios from "../api/axiosPrivate";
import useAuth from "./useAuth";

const useRefreshToken = () => {
    const { setUser } = useAuth();
    const refresh = async () => {
        const response = await axios.get("/user/refresh", {
            withCredentials: true
        });

        setUser(prev => {
            return { 
                ...prev, 
                token: response.data.token,
                roles: response.data.roles 
            }
        });
        return response.data.token;
    }

    return refresh;
}

export default useRefreshToken;