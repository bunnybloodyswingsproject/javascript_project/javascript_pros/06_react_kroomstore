import { useContext } from "react";
import { AuthScannerContext } from "../context/AuthScannerContext";

const useAuthScanner = () => {
    return useContext(AuthScannerContext);
}

export default useAuthScanner;