import React from 'react'
import axios from "../api/axiosPrivate";
import useAuth from './useAuth';

const useLogout = () => {
    const { setUser } = useAuth();
    const logout = async () => {
        setUser({
            email: "",
            image: "",
            refreshToken: "",
            roles: [],
            token: "",
            username: "",
            _id: ""
        });

        try {
            const response = await axios.get("/user/logout", {
                withCredentials: true
            });

            
        }catch(error) {
            console.log(error);   
        }
    }
    return logout;
}

export default useLogout