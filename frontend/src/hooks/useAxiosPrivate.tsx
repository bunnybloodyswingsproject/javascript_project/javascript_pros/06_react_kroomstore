import { AxiosError, AxiosInstance, AxiosResponse, InternalAxiosRequestConfig  } from 'axios';
import React, { useEffect } from 'react'
import axiosPrivate from '../api/axiosPrivate';
import useAuth from './useAuth';
import useRefreshToken from './useRefreshToken'

const useAxiosPrivate = () => {
  const refresh = useRefreshToken();
  const { user } = useAuth();

    const onRequest = (config: InternalAxiosRequestConfig ) => {
        if(!config.headers!["Authorization"]) {
            config.headers!["Authorization"] = `Bearer ${user.token}`;
        }

        return config;
    }
    
    const onRequestError = (error: AxiosError): Promise<AxiosError> => {
        return Promise.reject(error);
    }

    const onResponse = (response: AxiosResponse) => {
        return response;
    }

    const onResponseError = async (error: AxiosError) => {
        console.log(error);
        const prevRequest: any = error?.config;
        if(error?.response?.status === 403 && !prevRequest?.sent) {
            prevRequest.sent = true;
            const newAccessToken = await refresh();
            prevRequest.headers["Authorization"] = `Bearer ${newAccessToken}`;
            return axiosPrivate(prevRequest);
        }
        return Promise.reject(error);

    }

    useEffect(() => {
        const requestIntercept = axiosPrivate.interceptors.request.use(onRequest, onRequestError);
        const responseIntercept = axiosPrivate.interceptors.response.use(onResponse, onResponseError);

        return () => {
            axiosPrivate.interceptors.request.eject(requestIntercept);
            axiosPrivate.interceptors.response.eject(responseIntercept);
        }

    }, [user, refresh])

    return axiosPrivate;
}

export default useAxiosPrivate