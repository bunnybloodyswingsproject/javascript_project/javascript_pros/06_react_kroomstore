import React from 'react'
import { useNavigate } from 'react-router-dom';
import useAuth from './useAuth'
import axios from "../api/axiosPrivate";

const usePersistingAuth = () => {
    const { setUser } = useAuth();
    const navigate = useNavigate();

    const persistAuth = async () => {
        try {
            const response = await axios.get("/user/persistingauth", {
                withCredentials: true
            });

            if(!response.data.success) {
                console.log(response.data.message);
                navigate("/login")
            }
            
            setUser(response.data.foundUser);
            return response.data.foundUser;

        }catch(error) {
            console.log(error);
            navigate('/');
        }
    }

    return persistAuth;
}

export default usePersistingAuth;