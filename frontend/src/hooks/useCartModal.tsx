import { useContext } from "react";
import { CartModalContext } from "../context/CartModalContext";

const useCartModal = () => {
    return useContext(CartModalContext);
}

export default useCartModal;