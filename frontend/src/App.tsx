import { useEffect, useState } from "react"
import AuthenticationBubbles from "./components/AuthenticationBubbles"
import AuthScanner from "./components/AuthScanner"
import Bubbles from "./components/Bubbles"
import useAuthScanner from "./hooks/useAuthScanner"
import Login from "./pages/login/Login"
import { MultiFormatOneDReader, BarcodeFormat, BrowserMultiFormatReader } from "@zxing/library"
import { CameraDevices } from "./utils/Type_definition";
import { Routes, Route } from "react-router-dom";
import Layout from "./components/Layout"
import Page404 from "./pages/404/404"
import Home from "./pages/home/home";
import useAuth from "./hooks/useAuth"
import RequireAuth from "./components/RequireAuth"
import Revenues from "./pages/admin/revenues/revenues";
import Unauthorized from "./pages/unauthorized/unauthorized"
import PersistLogin from "./components/PersistLogin";

function App() {
  const { isOpenAuthScanner } = useAuthScanner();
  const [ selectedDeviceId, setSelectedDeviceId ] = useState<string>("");
  const [ devices, setDevices ] = useState<CameraDevices[]>([]);
  const codeReader = new BrowserMultiFormatReader();
  const [barcode, setBarcode] = useState('');

  useEffect(() => {
    codeReader
      .listVideoInputDevices()
      .then((videoInputDevices) => {
        // console.log(videoInputDevices);
        if(videoInputDevices.length >= 1) {
          setSelectedDeviceId(videoInputDevices[0].deviceId);
          setDevices(videoInputDevices);
        }
      })
  }, [selectedDeviceId])

  return (
    // <div className="App">

    // </div>
    <div className="App">
      { 
        isOpenAuthScanner &&  
          <AuthScanner 
            selectedDeviceId={selectedDeviceId}
            devices={devices}
            setSelectedDeviceId={setSelectedDeviceId}
            codeReader={codeReader}
            setBarcode={setBarcode}
          /> 
      }
      <AuthenticationBubbles />
      <Routes>
        <Route path="/" element={<Layout />}>
          {/* Public access */}
          <Route path="/login" element={<Login barcode={barcode} />} />
          <Route path="/unauthorized" element={<Unauthorized />} />

          {/* Protected Routes */}
          <Route element={<PersistLogin />}>
            <Route element={<RequireAuth allowedRoles={["5462", "5471"]} />}>
              <Route path="/" element={<Home />} />
            </Route>
            <Route element={<RequireAuth allowedRoles={["5462"]} />}>
              <Route path="/admin/revenues" element={<Revenues />} />
            </Route>
          </Route>

          <Route path="*" element={<Page404 />} />
        </Route>
      </Routes>
      <Bubbles 
            top="50%"
            left="50%" 
            width="160px"
            height="160px"
            color="linear-gradient(118.48deg, #EE97CC 19.36%, rgba(255, 255, 255, 0.9) 87.56%)"
            transformProperty='translate(-50%, -25%)'
      />
      <Bubbles 
            bottom="2vh"
            left="19vw" 
            width="250px"
            height="250px"
            color="linear-gradient(313.03deg, rgb(238, 151, 204) 19.86%, rgba(255, 255, 255, 0.9) 84.22%)"
            // transformProperty='translate(-50%, -25%)'
      />
      <Bubbles 
            bottom="-6vh"
            right="-4vw" 
            width="320px"
            height="320px"
            color="linear-gradient(313.03deg, #EE97CC 19.86%, rgba(255, 255, 255, 0.9) 84.22%)"
            // transformProperty='translate(-50%, -25%)'
      />
    </div>
  )
}

export default App
