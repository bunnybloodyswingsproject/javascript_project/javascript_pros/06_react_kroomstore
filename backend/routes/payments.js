const router = require("express").Router();

const {
    manual
} = require("../controllers/payments");

router.post("/manual_checkout", manual);

module.exports = router;