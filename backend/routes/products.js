const router = require("express").Router();
const verifyJWT = require("../middlewares/verifyJWT");
const verifyRoles = require("../middlewares/verifyRoles");

const {
    addProducts,
    getProducts
} = require("../controllers/products");

router.post("/add", verifyJWT, verifyRoles("5462"), addProducts);
router.get("/", verifyJWT, verifyRoles("5462"), getProducts);

module.exports = router;