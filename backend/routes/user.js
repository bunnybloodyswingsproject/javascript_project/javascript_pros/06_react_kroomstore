const router = require("express").Router();
const verifyJWT = require("../middlewares/verifyJWT");
const verifyRoles = require("../middlewares/verifyRoles");

const {
    refresh,
    logout,
    user_roles_update,
    persistingAuth
} = require("../controllers/user");

router.get("/refresh", refresh);
router.get("/logout", logout);
router.post("/update_user_roles", verifyJWT, verifyRoles("5462"), user_roles_update);
router.get("/persistingauth", persistingAuth);

module.exports = router;