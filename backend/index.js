require("dotenv").config();
const express = require("express");
const app = express();
const connectDB = require('./db/connection');
const authRoutes = require("./routes/auth");
const userRoutes = require("./routes/user");
const productRoutes = require("./routes/products");
const paymentRoutes = require("./routes/payments");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const credentials = require("./middlewares/credentials");
const corsOption = require("./config/corsOption");
app.use(express.urlencoded({
    extended: false
}));

app.use(express.json());
app.use(cookieParser());
// const corsOption = {
//     origin: "http://localhost:5173/",
//     credentials: true,
//     optionSuccess: 200
// }

app.use(credentials);
app.use(cors(corsOption));
app.use("/api/auth", authRoutes);
app.use("/api/user", userRoutes);
app.use("/api/products", productRoutes);
app.use("/api/payments", paymentRoutes);

app.listen(8000, () => {
    connectDB(process.env.MONGO_URI);
    console.log("Backend is Running.");
})