const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");

const ProductSchema = new mongoose.Schema({
    name: { type: String, required: true },
    description: { type: String, required: true },
    price: { type: Number, required: true },
    category: { type: String, required: true },
    quantity: { type: Number, default: 0 },
    image: { type: String, default: "" }
});

module.exports = mongoose.model("Product", ProductSchema);