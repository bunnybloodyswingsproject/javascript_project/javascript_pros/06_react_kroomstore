const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");

const orderSchema = new mongoose.Schema({
    userId: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    products: [
        {
            _id: {
                type: mongoose.Schema.Types.ObjectId
            }, 
            name: String,
            price: Number,
            category: String,
            quantity: Number
        }
    ],
    orderDate: { type: Number, default: Date.now },
});

module.exports = mongoose.model("Order", orderSchema);
  