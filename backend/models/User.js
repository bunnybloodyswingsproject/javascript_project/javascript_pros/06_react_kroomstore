const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");

const UserSchema = new mongoose.Schema({
    username: {
        type: String,
        required: [true, "Please provide your usersId"],
        maxLength: 20,
        minLength: 10,
        unique: true
    },
    email: {
        type: String,
        required: [true, "Please provide an email address."],
        unique: true
    },
    image: {
        type: String,
        default: ""
    },
    roles: {
        type: Array,
        default: []
    },
    refreshToken: {
        type: String,
        default: ""
    }
});

UserSchema.methods.createJWT = function () {
    return jwt.sign(
        {
            "UserInfo": {
                "username": this.username,
                roles: this.roles 
            }
        },
        process.env.SECRET_KEY,
        {expiresIn: "30 days"}
    )
}

UserSchema.methods.refreshJWT = function () {
    return jwt.sign(
        {
            "UserInfo": {
                "username": this.username,
                roles: this.roles 
            }
        },
        process.env.REFRESH_KEY,
        {expiresIn: "60 days"}
    )
}

module.exports = mongoose.model("User", UserSchema);