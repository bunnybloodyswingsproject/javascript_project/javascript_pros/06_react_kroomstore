const whitelistedAPI = require("./whitelistedAPI");

const corsOption = {
    origin: (origin, callback) => {
        if(whitelistedAPI.indexOf(origin) !== -1 || !origin) {
            callback(null, true)
        }else{
            callback(new Error("Not allowed by CORS"));
        }
    },
    optionSuccessStatus: 200
}

module.exports = corsOption;