const User = require("../models/User");

const register = async (req, res) => {
    try {
        const newUser = await User.create({...req.body});
        res.send({
            success: true,
            message: "Successfully added a user",
            newUser
        })
    }catch(error) {
        console.log(error);
        return res.send({
            success: false,
            message: "Somethings wrong with register."
        });
    }
}

const login = async (req, res) => {
    try {
        const user = await User.findOne({
            username: req.body.username
        });

        if(!user) {
            return res.send({
                success: false,
                message: "Email or password is not correct."
            });
        }

        let token = user.createJWT();
        let refreshToken = user.refreshJWT();

        await User.updateOne({
            username: req.body.username
        }, {
            $set: {
                refreshToken: refreshToken
            }
        });

        const { ...info } = user._doc;
        res.cookie("jwt", refreshToken, { httpOnly: true, maxAge: 2 * 30 * 24 * 60 * 60 * 1000, sameSite: "None", secure: true }); // sameSite: "None", secure: true - parameters for prod deployment
        
        res
            .status(200)
            .send({
                success: true,
                message: "Welcome Buddy! Lets have some money today.",
                user: { ...info, token }
            })

    }catch(error) {
        console.log(error);
        res.send({
            success: false,
            message: "Error with login functionality."
        })
    }
}

module.exports = {
    register,
    login
}