const Order = require("../models/Order")

const manual = async (req, res) => {
    console.log(req.body)
    try {
        const order = await Order.create({
            userId: req.body.userId,
            products: req.body.products
        });

        if(!order) {
            return res.send({
                success: false,
                message: "Failure in processing the order."
            });
        }

        res.send({
            success: true,
            message: "Thank you for patronizing our shop!"
        });
        
    }catch(error) {
        res.send({
            success: false,
            message: "There's something wrong with manual checkout."
        })
    }
}

module.exports = {
    manual
}