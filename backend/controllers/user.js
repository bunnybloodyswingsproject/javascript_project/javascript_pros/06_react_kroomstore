const User = require("../models/User");
const jwt = require("jsonwebtoken");
// const mongoose = require("mongoose");

const refresh = async(req, res) => {
    try {
        const cookies = req.cookies;
        if(!cookies?.jwt) return res.status(401).send({ success: false, message: "System cannot find the user token." });
    
        const refreshToken = cookies.jwt;
        const foundUser = await User.findOne({ refreshToken: refreshToken });

        if(!foundUser) return res.status(403).send({ success: false, message: `System cannot find the user with token ${refreshToken}`  });
        
        jwt.verify(
            refreshToken,
            process.env.REFRESH_KEY,
            (err, decoded) => {
                // console.log(decoded)
                if(err || foundUser.username !== decoded.UserInfo.username) return res.status(403).send({ success: false, message: "The system cannot find the refreshed users" });
                const token = jwt.sign(
                    {
                        "UserInfo": {
                            "username": decoded.username,
                            roles: foundUser.roles 
                        }
                    },
                    process.env.SECRET_KEY,
                    { expiresIn: "30 days" }
                );
                res.json({ token, roles: foundUser.roles });
            }
        )
    }catch(error) {
        console.log(error)
        res.send({
            success: false,
            message: "There's something wrong with refresh functionality."
        })
    }
}

const persistingAuth = async (req, res) => {
    const cookies = req.cookies;
    if(!cookies?.jwt) return res.status(401).json({ success: false, "message": "Cannot find users token" });
    const refreshToken = cookies.jwt;
    const foundUser = await User.findOne({ refreshToken });
    // console.log(foundUser)
    if(!foundUser) return res.status(403).send({ success: false, message: `System cannot find the user with token ${refreshToken}`  });

    jwt.verify(
        refreshToken,
        process.env.REFRESH_KEY,
        async (err, decoded) => {
            // console.log(err);
            if(err && foundUser.username !== decoded.UserInfo.username ) return res.status(403).send({ success: false, message: "The system cannot find the refreshed users" });
            
            return res.send({
                success: true,
                message: "Successfully fetched the data for persisting auth.",
                foundUser
            })
        }
    );   
}

const logout = async(req, res) => {
    try {
        const cookies = req.cookies;
        if(!cookies?.jwt) return res.status(204).send({ success: true, message: "You are currently logout in the system." });
        
        const refreshToken = cookies.jwt;
        const foundUser = User.find({ refreshToken: refreshToken });
        
        if(!foundUser) {
            return res
                    .clearCookie("jwt", { httpOnly: true, sameSite: "None", secure: true }) // sameSite: "None", secure: true - add this parameters for prod
                    .status(204)
                    .send({ success: false, message: `Cannot find the user with token ${refreshToken}`  });
        }

        await User.updateOne({
            refreshToken: refreshToken
        }, {
            $set: {
                refreshToken: ""
            }
        });

        return res
            .clearCookie("jwt", { httpOnly: true, sameSite: "None", secure: true })// secure: true - only serves on https; sameSite: "None", secure: true - use this for deployment
            .status(204)
            .send({
                success: true,
                message: "You successfully logout."
        });
    
    }catch(error) {
        console.log(error);
        return res.send({
            success: false,
            message: "there's something wrong with logout functionality."
        })
    }
}

const user_roles_update = async(req, res) => {
    // console.log(req);
    const { username, roles } = req.body;

    try {
        const user = await User.findOne({username});

        if(!user) {
            return res.send({
                success: false,
                message: "Username is not existing in the users database."
            })
        }

        await User.updateOne(
            {username},
            {
                $set: {
                    roles: roles
                }
            }
        );

        res.send({
            success: true,
            message: "You successfully updated the user roles."
        })

    }catch(error) {
        console.log(error);
        res.send({
            success: false,
            message: "Something wrong with user role update."
        })
    }
}

module.exports = {
    refresh,
    logout,
    user_roles_update,
    persistingAuth,
    persistingAuth
}