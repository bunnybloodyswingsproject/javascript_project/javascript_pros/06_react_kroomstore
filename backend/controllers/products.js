const Product = require("../models/Product");

const addProducts = async(req, res) => {

    try {
        const newProduct = await Product.create({...req.body});
        const updatedProduct = await Product.findOne({_id: newProduct._id});

        if(!updatedProduct) {
            return res.send({
                success: false,
                message: "Something wrong with updating added product."
            });
        }

        await Product.findOneAndUpdate(
            { _id: newProduct._id }
        , {
            $set: {
                arrival_date: Date.now(),
                expiration_date: Date.now() + 432000
            }
        });

        res.send({
            success: true,
            message: "Successfully added a product",
            newProduct
        })
    }catch(error) {
        console.log(error);
        res.send({
            success: false,
            message: "There's something wrong adding products functionality."
        })
    }
}

const getProducts = async (req, res) => {
    try {
        const products = await Product.find({ category: req.query.category });

        if(!products) {
            return res.send({
                success: false,
                message: "No product available",
                products
            });
        }

        res.send({
            success: true,
            message: "Successfully fetched all products.",
            products
        });

    }catch(error) {
        console.log(error);

        res.send({
            success: false,
            message: "There's something wrong with get products functionality."
        })
    }
}

module.exports = {
    addProducts,
    getProducts
}