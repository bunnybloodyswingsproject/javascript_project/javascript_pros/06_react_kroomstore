const verifyRoles = (...allowedRoles) => {
    return (req, res, next) => {
        if(!req?.roles) return res.status(401).send({ success: false, message: "Unauthorized role to access this function/page." });
        const rolesArray = [...allowedRoles];
        // console.log(rolesArray);
        // console.log(req.roles);
        const result = req.roles.map(role => rolesArray.includes(role)).find(value => value === true);
        if(!result) return res.status(401).send({ success: false, message: "You are not authorized to access this page/function."});
        next();
    }
}

module.exports = verifyRoles;