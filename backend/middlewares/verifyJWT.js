const jwt = require("jsonwebtoken");

const verifyJWT = (req, res, next) => {
    // console.log(req?.cookies)
    // const authHeader = req.headers["authorization"] || req.headers["Authorization"];
    const authHeader = req?.cookies?.jwt;
    if(!authHeader) return res.status(401).send({ "message": "Invalid token" });
    // const token = authHeader.split(" ")[1];
    const token = authHeader;
    // console.log(token)
    jwt.verify(
        token,
        process.env.REFRESH_KEY,
        (err, decoded) => {
            // console.log(decoded);
            if(err) return res.status(403).send({ "message": "Unauthorized personnel" });
            req.user = decoded.UserInfo.username;
            req.roles = decoded.UserInfo.roles
            next();
        }
    )
}

module.exports = verifyJWT;